import { Config } from "apollo-server-express";
import { DataTypes, ModelDefined } from "sequelize";
import { DEFAULT_LIMIT, DEFAULT_PAGE } from "../config/config";
import { Database } from "../db/db";
import { RestaurantClass } from "../entitites/restaurant.class";
import { Restaurant } from "../types/datasource";

const { DataSource } = require('apollo-datasource');

export class RestaurantDatasource extends DataSource {
    private database: Database;
    // models
    restaurantModel: ModelDefined<Restaurant[], any>;
    countryModel: ModelDefined<any, any>;
    restaurantHasImagesModel: ModelDefined<any, any>;
    

    /**
     * 
     * @param database Database connection (Sequelize instance)
     */
    constructor(database: Database) {
        super();
        // init database connection
        this.database = database;
        // load models
        this.loadModels();
    }

    /**
     * This is a function that gets called by ApolloServer when being setup.
     * This function gets called with the datasource config including things
     * like caches and context. We'll assign this.context to the request context
     * here, so we can know about the user making requests
     */
    initialize(config: Config) {
        this.context = config.context;
    }

    /**
     * List restaurants
     * 
     * @param restaurantName restaurant name
     * @param withImages list restaurants with images
     * 
     * @returns Array of restaurants (Promise)
     */
     list(restaurantName?: string, withImages = false, pagination = {
        limit: DEFAULT_LIMIT,
        offset: DEFAULT_PAGE
     }): Promise<{
         restaurants: Restaurant[],
         total: number
     }> {
        const whereCondition = restaurantName ? {
            name: restaurantName
        }: {};
        // Call database with associations
        return (this.restaurantModel.findAndCountAll({
            where: whereCondition,
            include: [
                {
                    model: this.countryModel,
                    as: 'country'
                },
                {
                    model: this.restaurantHasImagesModel,
                    as: 'imagesFromDB',
                    attributes: ['imageUuid'],
                    required: withImages
                }
            ],
            limit: pagination.limit,
            offset: pagination.offset
        }) as any).then(({
            count,
            rows
        }:{
            count: number,
            rows: Restaurant[]
        }) => {
            return {
                restaurants: rows.map((rest) => new RestaurantClass(rest)),
                total: count
            }
        }) as any;
    }

    /**
     * Load models
     */
    private loadModels(){
        // Restaurant model
        this.restaurantModel = this.database.connection
        .define('restaurant', {
            restaurantUuid:{
                field: 'restaurant_uuid',
                type: DataTypes.STRING,
                primaryKey: true
            },
            name:{
                type: DataTypes.STRING
            },
            countryCode:{
                field: 'country_code',
                type: DataTypes.STRING
            },

        }, {
            tableName: 'restaurant',
            timestamps: false
        });

        // Country model
        this.countryModel = this.database.connection
        .define('country', {
            code:{
                field: 'country_code',
                type: DataTypes.STRING,
                primaryKey: true
            },
            locales:{
                type: DataTypes.ARRAY(DataTypes.STRING)
            }

        }, {
            tableName: 'country',
            timestamps: false
        });

        // Restaurant has images model
        this.restaurantHasImagesModel = this.database.connection
        .define('restaurant_has_images', {
            restaurantUuid:{
                field: 'restaurant_uuid',
                type: DataTypes.STRING,
                primaryKey: true
            },
            imageUuid:{
                field: 'image_uuid',
                type: DataTypes.STRING,
                primaryKey: true
            },

        }, {
            tableName: 'restaurant_has_image',
            timestamps: false
        });

        // Define associations
        this.restaurantModel.belongsTo(this.countryModel, { foreignKey: 'countryCode'});
        this.restaurantModel.hasMany(this.restaurantHasImagesModel, {foreignKey: 'restaurantUuid', as: 'imagesFromDB'});
    }
}