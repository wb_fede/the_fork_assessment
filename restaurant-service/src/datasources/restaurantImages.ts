import { RESTDataSource } from "apollo-datasource-rest";
import { Images } from "../types/datasource";

export class RestaurantImagesDatasource extends RESTDataSource {
    /**
     * 
     * @param url base url
     */
    constructor(url: string) {
        super();
        // init database connection
        this.baseURL = url;
    }

    /**
     * List images
     */
    list(): Promise<Images[]>{
        return this.get(this.baseURL as string).then((response: {
            images: Images[]
        }) => response.images || []);
    }
}