import { Restaurant } from "../types/datasource";

export class Response{
    restaurants: Restaurant[];
    pagination:{
        total: number,
        pageCount: number,
        currentPage: number
    } = {
        total: 5,
        pageCount: 0,
        currentPage: 0
    };

    constructor(restaurants: Restaurant[], pagination:{
        limit: number,
        offset: number,
        total: number
    }){
        this.restaurants = restaurants;
        this.pagination.total = pagination.total;
        this.pagination.pageCount = Math.ceil(this.pagination.total / (pagination.limit || 1));
        this.pagination.currentPage = pagination.offset;
    }
}