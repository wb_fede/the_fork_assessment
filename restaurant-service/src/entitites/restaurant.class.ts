import { FR_LOCALE } from "../config/config";
import { Country, Restaurant } from "../types/datasource";

export class RestaurantClass{
    restaurantUuid: string;
    name: string;
    country?: Country;
    images?: string[];
    imagesFromDB?: {
        imageUuid: string;
    }[];
    allowReview?: boolean;
    
    constructor({
        restaurantUuid,
        name,
        country,
        imagesFromDB
    }: Restaurant){
        this.restaurantUuid = restaurantUuid;
        this.name = name;
        this.country = country;
        this.imagesFromDB = imagesFromDB;
        this.images = this.imagesFromDB?.map((img) => img.imageUuid);
        //The field "allowReview" is not stored anywhere. It must be computed "on the fly" and set to true for french restaurants only.
        this.allowReview = (this.country?.locales?.indexOf(FR_LOCALE) || -1) >= 0;
    }
}