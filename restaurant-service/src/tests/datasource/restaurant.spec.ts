import { RestaurantDatasource } from "../../datasources/restaurants";

const databaseMock = {
    connection:{
        define: jest.fn(() => {
            return {
                belongsTo: jest.fn(),
                hasMany: jest.fn(),
            }
        })
    }
} as any;

describe('Restaurant datasource', () => {
    describe('List', () => {
        it('It should call database', async() => {
            const findAndCountAllMock = jest.fn(() => Promise.resolve({
                count: 10,
                rows: []
            }));
            const restaurantDatasource = new RestaurantDatasource(databaseMock);

            restaurantDatasource.restaurantModel = {
                findAndCountAll: findAndCountAllMock as any
            } as any;

            await restaurantDatasource.list();

            expect(findAndCountAllMock).toHaveBeenCalled();
        });

        it('If request has name, it should filter restaurants by name', async() => {
            const findAndCountAllMock = jest.fn(() => Promise.resolve({
                count: 10,
                rows: []
            }));
            const restaurantDatasource = new RestaurantDatasource(databaseMock);

            restaurantDatasource.restaurantModel = {
                findAndCountAll: findAndCountAllMock as any
            } as any;

            await restaurantDatasource.list('test');

            const mockArgs = findAndCountAllMock.mock.calls[0] as any;

            expect(mockArgs[0].where).toStrictEqual({"name": "test"})
        });

        it('If request has withImages arg, it should filter restaurants by images', async() => {
            const findAndCountAllMock = jest.fn(() => Promise.resolve({
                count: 10,
                rows: []
            }));
            const restaurantDatasource = new RestaurantDatasource(databaseMock);

            restaurantDatasource.restaurantModel = {
                findAndCountAll: findAndCountAllMock as any
            } as any;

            await restaurantDatasource.list('test', true);

            const mockArgs = findAndCountAllMock.mock.calls[0] as any;

            expect(mockArgs[0].include[1].required).toBeTruthy();
        });

        it('If request has limit and offset, it should paginate', async() => {
            const findAndCountAllMock = jest.fn(() => Promise.resolve({
                count: 10,
                rows: []
            }));
            const restaurantDatasource = new RestaurantDatasource(databaseMock);

            restaurantDatasource.restaurantModel = {
                findAndCountAll: findAndCountAllMock as any
            } as any;

            await restaurantDatasource.list('test', true, {
                limit: 10,
                offset: 5
            });

            const mockArgs = findAndCountAllMock.mock.calls[0] as any;

            expect(mockArgs[0].limit).toBe(10);
            expect(mockArgs[0].offset).toBe(5);
        });

        it('It should map response', (done) => {
            const findAndCountAllMock = jest.fn(() => Promise.resolve({
                count: 10,
                rows: []
            }));
            const restaurantDatasource = new RestaurantDatasource(databaseMock);

            restaurantDatasource.restaurantModel = {
                findAndCountAll: findAndCountAllMock as any
            } as any;

            restaurantDatasource.list('test', true, {
                limit: 10,
                offset: 5
            }).then(({restaurants, total}) => {
                expect(restaurants.length).toBe(0);
                expect(total).toBe(10);
                done();
            });
        });
    });
});