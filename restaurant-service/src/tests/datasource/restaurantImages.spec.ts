import { RestaurantImagesDatasource } from "../../datasources/restaurantImages";

const mockGet = jest.fn(() => Promise.resolve({
    images: ['image1', 'image2']
}));

jest.mock('apollo-datasource-rest', () => {
  class MockRESTDataSource {
    baseUrl = ''
    get = mockGet
  }
  return {
    RESTDataSource: MockRESTDataSource,
  }
})

describe('restaurantImages datasource', () => {
    describe('Constructor', () => {
        it('Constructor should set base url', () => {
            const obj = new RestaurantImagesDatasource('url');
    
            expect(obj.baseURL).toBe('url');
        });
    })

    describe('List method', () => {
        it('It should call service', async() => {
            const obj = new RestaurantImagesDatasource('url');

            await obj.list();

            expect(mockGet).toBeCalledWith('url')
        });

        it('It should return only images', async() => {
            const obj = new RestaurantImagesDatasource('url');

            const result = await obj.list();

            expect(result).toStrictEqual(['image1', 'image2']);
        });
    });
});