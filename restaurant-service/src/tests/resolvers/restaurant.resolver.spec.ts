import { restaurantResolver } from "../../resolvers/restaurants.resolver";

const listRestaurantsMock = jest.fn(() => Promise.resolve({
    restaurants: [{
        images: ['1']
    }],
    total: 1
}));

const listImagesMock = jest.fn(() => Promise.resolve([{
    imageUuid: '1',
    url: 'url'
}]));

const contextMock = {
    dataSources: {
        restaurantDatasource: {
            list: listRestaurantsMock
        },
        restaurantImagesDatasource:{
            list: listImagesMock
        }
    }
} as any;

describe('Restaurant resolver', () =>  {
    it('It should call services', () => {
        restaurantResolver.Query.list({}, {
            name: "name",
            limit: 1,
            page: 1,
            withImages: true
        }, contextMock);

        expect(listRestaurantsMock).toHaveBeenCalled();
        expect(listImagesMock).toHaveBeenCalled();
    });

    it('It should return images urls', (done) => {
        restaurantResolver.Query.list({}, {
            name: "name",
            limit: 1,
            page: 1,
            withImages: true
        }, contextMock).then(result => {
            expect(result.restaurants[0].images).toStrictEqual(['url']);
            done();
        });
    })
})