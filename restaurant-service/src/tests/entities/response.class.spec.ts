import { RestaurantClass } from "../../entitites/restaurant.class";
import { Response } from "../../entitites/response.class";
import { Restaurant } from "../../types/datasource";

const dummyRestaurant: Restaurant = {
    restaurantUuid: 'restaurantUuid',
    name: 'name',
    country: {
        code: 'code',
        locales: []
    },
    imagesFromDB: [],
    images: [],

}

describe('Response class', () => {
    it('It should create class', () => {
        const responseObj = new Response([new RestaurantClass(dummyRestaurant)], {
            limit: 1,
            offset: 1,
            total: 10
        });

        expect(responseObj.restaurants.length).toBe(1);
        expect(responseObj.restaurants[0].name).toBe('name');
        expect(responseObj.pagination.currentPage).toBe(1);
        expect(responseObj.pagination.pageCount).toBe(10);
        expect(responseObj.pagination.total).toBe(10);
    });

    it('It should calculate page count dividing total / limit', () => {
        const responseObj = new Response([new RestaurantClass(dummyRestaurant)], {
            limit: 5,
            offset: 1,
            total: 10
        });
        expect(responseObj.pagination.pageCount).toBe(2);
    });

    it('If the limit is 0, it should take default value as 1', () => {
        const responseObj = new Response([new RestaurantClass(dummyRestaurant)], {
            limit: 0,
            offset: 1,
            total: 10
        });
        expect(responseObj.pagination.pageCount).toBe(10);
    });
});