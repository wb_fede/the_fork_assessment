import { RestaurantClass } from "../../entitites/restaurant.class";
import { Restaurant } from "../../types/datasource";

const dummyRestaurant: Restaurant = {
    restaurantUuid: 'restaurantUuid',
    name: 'name',
    country: {
        code: 'code',
        locales: []
    },
    imagesFromDB: [],
    images: [],

}

describe('Restaurant class', () => {
    it('It should create class', () => {
        const responseObj = new RestaurantClass(dummyRestaurant);

        expect(responseObj.restaurantUuid).toBe('restaurantUuid');
        expect(responseObj.name).toBe('name');
        expect(responseObj.country?.code).toBe('code');
        expect(responseObj.country?.locales).toStrictEqual([]);
    });
});