export const databaseConnection = {
    "database": "thefork",
    "host": "postgres",
    "user": "postgres",
    "password": "postgres",
    "port": 5432
}

export const imagesEndpoint = 'http://image-service:3010/images';

export const DEFAULT_LIMIT = 10;
export const DEFAULT_PAGE = 0;

export const FR_LOCALE = 'fr_FR';