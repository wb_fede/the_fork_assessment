import { gql } from "apollo-server-express";
import { readFileSync } from "fs";
import path from "path";

// Combine all graphql in one single constant
export const typeDefs = gql`
  ${readFileSync(path.resolve(__dirname, "pagination.graphql").toString())}
  ${readFileSync(path.resolve(__dirname, "country.graphql").toString())}
  ${readFileSync(path.resolve(__dirname, "restaurant.graphql").toString())}

  type Query {
    list(name: String, withImages: Boolean, page: Int, limit: Int): RestaurantResult
  }
`;