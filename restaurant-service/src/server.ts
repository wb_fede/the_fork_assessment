import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import config from 'config';
import { resolvers } from './resolvers/index';
import { typeDefs } from './schemas/index';
import { RestaurantDatasource } from './datasources/restaurants';
import { Database } from './db/db';
import { databaseConnection, imagesEndpoint } from './config/config';
import { RestaurantImagesDatasource } from './datasources/restaurantImages';

// Init server
const main = async () => {
  const app = express();

  // Database connection
  const database = new Database(
    databaseConnection.database,
    databaseConnection.user,
    databaseConnection.password,
    databaseConnection.host,
  );

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => ({
      restaurantDatasource: new RestaurantDatasource(database),
      restaurantImagesDatasource: new RestaurantImagesDatasource(imagesEndpoint)
    }),
  });

  await server.start();

  server.applyMiddleware({ app });

  app.listen({ port: config.get('server.port') }, () => console.info(
    `🚀 Server ready and listening at ==> http://localhost:${config.get('server.port')}${
      server.graphqlPath
    }`,
  ));
};

main().catch((error) => {
  console.error('Server failed to start', error);
});
