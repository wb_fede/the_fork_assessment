import { restaurantResolver } from "./restaurants.resolver";

// Merge all resolvers in one single constant
export const resolvers = {
    ...restaurantResolver
};