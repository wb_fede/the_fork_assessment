import { DEFAULT_LIMIT, DEFAULT_PAGE } from '../config/config';
import { Response } from '../entitites/response.class';
import { Context } from '../types/datasource';
// Restaurant Resolver
export const restaurantResolver = {
    Query: {
        list(_: Object, args: { 
            name: string,
            withImages: boolean,
            limit: number,
            page: number
        }, context: Context){
            // Get arguments
            const { 
                name,
                withImages = false,
                limit = DEFAULT_LIMIT,
                page = DEFAULT_PAGE
             } = args;
            // list restaurants and images in parallel
            return Promise.all([
                context.dataSources.restaurantDatasource.list(name, withImages, {
                    limit: limit,
                    offset: page
                }),
                context.dataSources.restaurantImagesDatasource.list()
            ]).then(([{ restaurants, total }, images]) => {
                // create map
                const imageMap = images.reduce((map, element) => {
                    map.set(element.imageUuid, element.url);
                    return map;
                }, new Map());

                // convert imageUUid to url
                restaurants.forEach(rest => {
                    rest.images = rest.images?.map(img => {
                        return imageMap.get(img);
                    }).filter(v => v);
                })
                // return list
                return new Response(restaurants, {
                    limit: limit,
                    offset: page,
                    total: total
                });
            });
            
        }
    },
};