import { RestaurantImagesDatasource } from "../datasources/restaurantImages";
import { RestaurantDatasource } from "../datasources/restaurants"

export type DataSource = {
    restaurantDatasource: RestaurantDatasource,
    restaurantImagesDatasource: RestaurantImagesDatasource
}

export type Context = {
    dataSources: DataSource
};

export type Restaurant = {
    restaurantUuid: string,
    name: string,
    country?: Country,
    images?: string[],
    imagesFromDB?: {
        imageUuid: string;
    }[];
    allowReview?: Boolean;
}

export type Country = {
    code: string,
    locales: string[]
}

export type Images = {
    imageUuid: string;
    url: string;
}