# TheFork backend test v1.1

## Goal
Creation of a graphql API in NodeJS.

## Context
The purpose of this project is to create a simple graphQL API in charge of exposing Restaurant information fetched from an external service (`image-service`) and from a local database.

### Infrastructure
Infrastructure is available in the [docker-compose file](./docker-compose.yml) and composed as follow:
 - a NodeJs GraphQL micro-service ([restaurant-service](./restaurant-service)) You can use the playground to test it http://localhost:3000/graphql
 - a PostgreSQL database
 - a REST micro-service ([image-service](./image-service))

### Database

Database tables are created through migrations and the database is populated with test data through seeds (you don't need to modify migrations and seed directories)

> - restaurant (restaurant_uuid, name, country_code)
> - restaurant_has_images (restaurant_uuid, image_uuid)
> - country (country_code, locales) - (locales is an psql array of texts)


### Image service
This service is exposing 2 endpoints:
- `GET /images` returning all images (no filter)

Expected response:
```json
{
    "images": [
        {
            "imageUuid": "imageUuid",
            "url": "url"
        }
    ]
}
```

## Project Structure
The folder structure of this app is explained below:

| Name | Description |
| ------------------------ | --------------------------------------------------------------------------------------------- |
| **config**               | Constants and config variables  |
| **datasources**          | RestaurantImages API and restaurant db connection  |
| **db**                   | Standard DB connection service  |
| **entities**             | Classes  |
| **resolvers**            | Graphql Resolvers  |
| **schemas**              | Graphql Schemas  |
| **types**                | Types  |
| **node_modules**         | Contains all  npm dependencies                                                            |
| **server.ts**            | Entry point to app                                                               |
| **tests**                | Tests                                                              |
| package.json             | Contains npm dependencies as well as [build scripts](#what-if-a-library-isnt-on-definitelytyped)   | 

## Getting started
- Install dependencies
```
cd <project_name>
npm install
```
- Build and run the project
```
docker-compose up
```

## Run tests
```
    npm run test
```

## Exercise description

Assessment description available in "README.md"

## Grahpql query
```
query{
  list{
    restaurants{
       restaurantUuid
        name
        country {
          code
          locales
        }
        images
        allowReview
    }
    pagination{
      total,
      pageCount,
      currentPage
    }
  }
}
```

```
{
  "data": {
    "list": {
      "restaurants": [
        {
          "restaurantUuid": "c49f90b5-4854-4841-9d97-265655ce2702",
          "name": "Silver River",
          "country": {
            "code": "ES",
            "locales": [
              "es_ES",
              "ca_ES",
              "eu_ES",
              "gl_ES"
            ]
          },
          "images": [
            "https://media.giphy.com/media/Ju7l5y9osyymQ/giphy.gif"
          ],
          "allowReview": false
        }
      ],
      "pagination": {
        "total": 50,
        "pageCount": 50,
        "currentPage": 2
      }
    }
  }
}
```

## Pagination and parameters
```
query{
  list(name:"test", withImages:true, limit: 10, page: 1) {
    restaurants{
       restaurantUuid
        name
        country {
          code
          locales
        }
        images
        allowReview
    }
    pagination{
      total,
      pageCount,
      currentPage
    }
  }
}
```

## Optimization list (TBD)
- Add more testing (unit, Integration, e2e, etc)
- Images service: return a list by uuid instead of an array
- Error handler (Public/private API)
- Data validation

## Author
Federico Devoto